package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Product;
@Service
public class Productdao {
@Autowired
ProductRepository productrepository;
public List<Product> getAllProducts()
{
	return productrepository.findAll();
}
public Product getProductById(int productId)
{
	return productrepository.findById(productId).orElse(null);
}
public Product getProductByName(String productName)
{
	return productrepository.findByName(productName);
}
public Product registerProduct(Product product)
{
	return productrepository.save(product);
	
}
public Product updateProduct(Product product)
{
	return productrepository.save(product);
}
public void deleteProduct(int productId)
{
	productrepository.deleteById(productId);
}
}
