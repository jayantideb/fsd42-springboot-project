package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;

@Entity
public class Customer {
      @Id@GeneratedValue
      private int customerId;
      @Column(name="customerName",length=40)
      private String customerName;
      private Date dob;
      private String address;
      private String city;
      private String country;
      @Column(name="phonenumber",length=10)
      private long phonenumber;
      private String emailId;
      private String password;
      @ManyToOne
      @JoinColumn(name="productId")
      Product product;
	Customer()
      {
    	  super();
      }
      Customer(int customerId,String customerName,Date dob,String address,String city,String country,long phonenumber,String emailId,String password)
      {
    	  this.customerId=customerId;
    	  this.customerName=customerName;
    	  this.dob=dob;
    	  this.address=address;
    	  this.city=city;
    	  this.country=country;
    	  this.phonenumber=phonenumber;
    	  this.emailId=emailId;
    	  this.password=password;
    	  
      }
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public long getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(long phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;}
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", dob=" + dob + ", address=" + address + ", city=" + city
				+ ", country=" + country + ", phonenumber=" + phonenumber + ", emailId=" + emailId + ", password="
				+ password +",product="+product+ "]";
	}
	
      
}
