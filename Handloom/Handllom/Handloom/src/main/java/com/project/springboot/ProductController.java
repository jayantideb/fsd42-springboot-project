package com.project.springboot;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.Productdao;
import com.model.Product;
@RestController
public class ProductController {
    @Autowired
    Productdao proddao;
    @GetMapping("/getAllProducts")
    public List<Product> getAllProducts()
    {
	return proddao.getAllProducts();
    // return proddao.getAllProducts();
    }
    @GetMapping("/getProductById/{productId}")
    public Product getProductById(@PathVariable("productId") int productId)
    {
	 return proddao.getProductById(productId);
    }
    @GetMapping("/getProductByName/{productName}")
    public Product getProductByName(@PathVariable("productName") String productName)
    {
	return proddao.getProductByName(productName);
    }
    @PostMapping("/registerProduct")
    public Product registerProduct(@RequestBody Product product)
    {
    	return proddao.registerProduct(product);
    }
    @PutMapping("/updateProduct")
    public Product updateProduct(@RequestBody Product product)
    {
    	return proddao.updateProduct(product);
    }
    @DeleteMapping("/deleteProduct/{productId}")
    public String deleteProduct(@PathVariable("productId") int productId)
    {
    	proddao.deleteProduct(productId);
    	return "Product Id: "+productId+" deleted successfully";
    }
}
