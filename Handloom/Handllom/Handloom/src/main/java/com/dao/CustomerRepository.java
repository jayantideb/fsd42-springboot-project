package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer,Integer>{
	@Query("from Customer p where p.customerName=:customerName")
	Customer findByName(@Param("customerName") String customerName);	
	@Query("from Customer e where e.emailId = :emailId and e.password = :password")
	public Customer login(@Param("emailId") String emailId, @Param("password") String password);
	

}
