package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Product {
     @Id@GeneratedValue
     private int productId;
     @Column(name="productName",length=20)
     private String productName;
     private double price;
     private String imagePath;
     private String description;
     @JsonIgnore
     @OneToMany(mappedBy="product")
     List <Customer> customerlist = new ArrayList <Customer>();
     Product()
     {
    	 super();
     }
     Product(int productId,String productName,double price,String imagePath,String description)
     {
    	 this.productId=productId;
    	 this.productName=productName;
    	 this.price=price;
    	 this.imagePath=imagePath;
    	 this.description=description;
    	 
     }
	public  int getProductId() {
		return productId;
	}
	public  void setProductId(int productId) {
		this.productId = productId;
	}
	
	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", price=" + price + ", imagePath="
				+ imagePath + ", description=" + description + ", customerlist=" + customerlist + "]";
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Customer> getCustomerlist() {
		return customerlist;
	}
	public void setCustomerlist(List<Customer> customerlist) {
		this.customerlist = customerlist;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
}


